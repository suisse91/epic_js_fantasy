/*
** get_next_line.h for gnl in /home/elkaim_r/rendu/get_next_line-2018-elkaim_r
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Tue Nov 19 10:44:58 2013 elkaim_r
** Last update Mon Jan  6 17:42:46 2014 elkaim_r
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_
# define READ_SIZE 100000

typedef struct	s_stuff
{
  long long	cursor;
  char		*buffer;
  long long	ret;
  long long	k;
  char		*minibuf;
  char		*line;
  int		fd;
}		t_stuff;

char	*my_realloc_str(char *dest, char *src, int k);
int	seekline(long long, char *, t_stuff *, int);
int	dumpline(long long *, char *, char *);
int	dumpcheck(char *);
char	*get_next_line(const int);

#endif /* !GET_NEXT_LINE_H_ */

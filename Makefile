##
## Makefile for Makefile in /home/elkaim_r/rendu/PSU_2013_my_ls
## 
## Made by elkaim_r
## Login   <elkaim_r@epitech.net>
## 
## Started on  Fri Nov 22 16:19:52 2013 elkaim_r
## Last update Sat May 10 16:20:00 2014 elkaim_r
##

#CFLAGS		= -W -Wall -Wextra -g3

CC		= gcc

NAME		= js_fantasy

NAMEEDITOR	= epic_editor

NAMECLIENT	= fantasy_client

NAMESERVER	= js_server

SRCEDITOR	= main_editor.c \
		get_next_line.c \
		list.c

SRCCLIENT	= main_client.c

SRCSERVER	= main_server.c \
		  my_strcmp.c

OBJEDITOR	= $(SRCEDITOR:.c=.o)

OBJCLIENT	= $(SRCCLIENT:.c=.o)

OBJSERVER	= $(SRCSERVER:.c=.o)

all		:	$(NAME)

$(NAME)		:	$(NAMEEDITOR) $(NAMECLIENT) $(NAMESERVER)

$(NAMEEDITOR)	:	$(OBJEDITOR)
			$(CC) -o $(NAMEEDITOR) $(OBJEDITOR)

$(NAMESERVER)	:	$(OBJSERVER)
			$(CC) -o $(NAMESERVER) $(OBJSERVER) -L./ -lNetworkBocalServer

$(NAMECLIENT)	:	$(OBJCLIENT)
			$(CC) -o $(NAMECLIENT) $(OBJCLIENT) -L./ -lNetworkBocalClient

clean		:
			rm -rf $(OBJEDITOR) $(OBJCLIENT) $(OBJSERVER)


fclean		:	clean
			rm -rf $(NAMEEDITOR) $(NAMESERVER) $(NAMECLIENT)

re		:	fclean all


## commandes de mat
spe		:	clear $(NAMEEDITOR) disp $(NAMECLIENT) disp2 $(NAMESERVER) disp3

clear		:
			clear
			@echo -e '\n\n\033[0;32m (1/3) >> compilation de :\033[0;35m' $(NAMEEDITOR) '\033[0m'

disp		:
			@echo -e '\n\033[0;32m (2/3) >> compilation de :\033[0;35m' $(NAMECLIENT) '\033[0m'

disp2		:
			@echo -e '\n\033[0;32m (3/3) >> compilation de :\033[0;35m' $(NAMESERVER) '\033[0m'

disp3		:
			@echo -e '\n\033[0;32m la compilation a réussi, suppression des fichiers temporaires\033[0m'
			rm -rf $(OBJEDITOR) $(OBJCLIENT) $(OBJSERVER)
			@echo '\n'
			@ls -a --color

.PHONY	: all clean fclean fclean re spe clear disp disp2 disp3

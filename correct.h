/*
** header.h for header in /home/raynau_a/rendu/GETNEXT-MAKEFILE-LIB
** 
** Made by a
** Login   <raynau_a@epitech.net>
** 
** Started on  Thu Jan  2 20:33:52 2014 a
** Last update Sat May 10 16:33:16 2014 mat
*/

#ifndef HEADER_H_
# define HEADER_H_

/*
** correct.c
*/
void	erase_exess(char *str);
void	eliminate_all_tabs(char **str);
int	no_useless_spaces(char **str);
void	eliminate_first_space(char **str);
void	eliminate_last_space(char **str);

#endif /* !HEADER_H_ */

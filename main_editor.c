/*
** main.c for game_editor in /home/elkaim_r/Documents/epic_js_fantasy
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Sat May 10 12:05:42 2014 elkaim_r
** Last update Sat May 10 16:38:15 2014 elkaim_r
*/

#include <string.h>
#include <stdio.h>
#include "get_next_line.h"
#include "editor.h"

void	fill_header(t_header *header)
{
  char	*res;

  printf("header's name>");
  fflush(stdout);
  res = get_next_line(0);
  header->name = res;
  printf("last room>");
  fflush(stdout);
  res = get_next_line(0);
  header->end_room = res;
  printf("start room>");
  fflush(stdout);
  res = get_next_line(0);
  header->start_room = res;
}

void	handle_request(char *input, t_game *game)
{
  if (!strcmp(input, "HEADER") || !strcmp(input, "header"))
    fill_header(&game->head);
}

int		main()
{
  char		*res;
  t_game	game;
  int		fd;

  init_header(game.header);
  printf("editor>");
  fflush(stdout);
  res = get_next_line(0);
  while (strcmp(res, "done"))
    {
      handle_request(res, &game);
      printf("editor>");
      fflush(stdout);
      res = get_next_line(0);
    }
  printf("%s\n%s\n%s\n", game.head.name, game.head.end_room, game.head.start_room);
  return (0);
}

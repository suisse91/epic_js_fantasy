/*
** network_server.h for network_server.h in /home/js/rush_bocal/lib
**
** Made by thibau_j
** Login   <thibau_j@epitech.net>
**
** Started on  Mon May  5 21:47:05 2014 thibau_j
** Last update Sat May 10 15:28:01 2014 ledara_f
*/

#ifndef NETWORK_SERVER_H_
# define NETWORK_SERVER_H_

# define FD_FREE 0
# define FD_CLIENT 1
# define FD_SERVER 2
# define MAX_FD 255

typedef void(*fct)();

typedef struct	s_env
{
  int		fd_type[MAX_FD];
  fct		fct_read[MAX_FD];
  fct		fct_write[MAX_FD];
  int		port;
  int		mode[MAX_FD];
  size_t	size[MAX_FD];
  char		*tab[MAX_FD];
  void		(*listen_read)(char *buff, size_t size, int id);
}		t_env;

typedef struct s_champ
{
  char		*name;
  char		*type;
  int		hp;
  int		spe;
  int		speed;
  int		deg;
  char		*weapon;
  char		*armor;
}		t_champ;

typedef struct s_monster
{
  char		*type;
  int		hp;
  int		spe;
  int		speed;
  int		deg;
  char		*weapon;
  char		*armor;
}		t_monster;

typedef struct s_room
{
  char		*name;
  char		*adv;
  char		*connect;
  char		*monster;
}		t_room;

typedef struct s_header
{
  int		magic;
  char		*name;
  char		*end;
  char		*start;
}		t_header;

typedef struct s_file
{
  t_champ	**c;
  t_monster	**m;
  t_room	**room;
  t_header	header;
}		t_file;

/*
** Port can be set but if you put 0 it will be set at 4242.
*/

void			server_loop(int port, void (*listen_read)
				    (char *buff, size_t size, int id));

#endif /* !NETWORK_SERVER_H_ */

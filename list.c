/*
** list.c for list in /home/amstut_a/rendu/epic_js_fantasy
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Sat May 10 15:48:03 2014 amstut_a
** Last update Sat May 10 16:39:35 2014 amstut_a
*/

#include <stdlib.h>
#include "editor.h"

t_champ		*add_elem_champ(t_champ *list)
{
  t_champ	*elem;
  t_champ	*tmp;

  tmp = list;
  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (list);
  elem->name = NULL;
  elem->type = NULL;
  elem->hp = 0;
  elem->spe = 0;
  elem->speed = 0;
  elem->deg = 0;
  elem->weapon = NULL;
  elem->armor = NULL;
  elem->next = NULL;
  if (list == NULL)
    return (elem);
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = elem;
  return (list);
}

t_monster	*add_elem_monster(t_monster *list)
{
  t_monster	*elem;
  t_monster	*tmp;

  tmp = list;
  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (list);
  elem->type = NULL;
  elem->hp = 0;
  elem->spe = 0;
  elem->speed = 0;
  elem->deg = 0;
  elem->weapon = NULL;
  elem->armor = NULL;
  elem->next = NULL;
  if (list == NULL)
    return (elem);
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = elem;
  return (list);
}

t_room		*add_elem_room(t_room *list)
{
  t_room	*elem;
  t_room	*tmp;

  tmp = list;
  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (list);
  elem->name = NULL;
  elem->adv = NULL;
  elem->tab_connection = NULL;
  elem->tab_monster = NULL;
  elem->next = NULL;
  if (list == NULL)
    return (elem);
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = elem;
  return (list);
}

void		init_header(t_header *header)
{
  if ((header = malloc(sizeof(*header))) == NULL)
    exit(EXIT_FAILURE);
  header->name = NULL;
  header->end_room = NULL;
  header->start_room = NULL;
}

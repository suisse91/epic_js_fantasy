/*
** gln.c for getnextline in /home/elkaim_r/rendu/get_next_line-2018-elkaim_r
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Wed Nov 20 10:41:40 2013 elkaim_r
** Last update Sat May 10 13:57:06 2014 elkaim_r
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "get_next_line.h"

char		*my_realloc_str(char *dest, char *src, int k)
{
  long long	i;
  long long	j;
  char		*res;

  i = 0;
  j = 0;
  while (dest[k] != 0)
    k = k + 1;
  res = malloc(sizeof(char) * (k + (READ_SIZE + 2)));
  if (res == NULL)
    return (NULL);
  while (dest[i] != 0)
    {
      res[i] = dest[i];
      i = i + 1;
    }
  while (src[j] != 0)
    {
      res[i] = src[j];
      i = i + 1;
      j = j + 1;
    }
  res[i] = 0;
  free(dest);
  return (res);
}

int	seekline(long long cursor, char *buffer, t_stuff *gen, int flag)
{
  if (buffer == NULL)
    return (-1);
  if (flag == 0)
    {
      while (buffer[cursor] != '\n' && buffer[cursor] != 0)
	cursor = cursor + 1;
      return (cursor + 1);
    }
  else
    {
      while (dumpcheck(gen->minibuf) == 0 && gen->ret != 0)
	{
	  gen->ret = read(gen->fd, gen->minibuf, READ_SIZE);
	  if (gen->ret == -1 || gen->minibuf == NULL)
	    return (-1);
	  gen->minibuf[gen->ret] = 0;
	  gen->buffer = my_realloc_str(gen->buffer, gen->minibuf, 0);
	  if (gen->buffer == NULL)
	    return (-1);
	  gen->k = gen->k + gen->ret;
	}
    }
}

int		dumpline(long long *cursor, char *buffer, char *line)
{
  long long	i;

  i = 0;
  if (line == NULL || buffer == NULL)
    return (-1);
  while (buffer[*cursor] != '\n' && buffer[*cursor] != '\0')
    {
      line[i] = buffer[*cursor];
      *cursor = *cursor + 1;
      i = i + 1;
    }
  line[i] = 0;
  *cursor = *cursor + 1;
}

int			dumpcheck(char *minibuf)
{
  static long long	i = 0;

  while (minibuf[i] != 0)
    {
      if (minibuf[i] == '\n')
	{
	  i = i + 1;
	  return (1);
	}
      i = i + 1;
    }
  if (minibuf[i] == 0)
    i = 0;
  return (0);
}

char			*get_next_line(const int fd)
{
  static  t_stuff	gen = {0, NULL, READ_SIZE, 0, NULL, NULL};

  gen.fd = fd;
  if (READ_SIZE < 1)
    return (NULL);
  if (gen.buffer == NULL)
    {
      if ((gen.buffer = malloc(sizeof(char) * (READ_SIZE + 2))) == NULL)
	return (NULL);
      memset(gen.buffer, 0, READ_SIZE + 2);
      if ((gen.minibuf = malloc(sizeof(char) * (READ_SIZE + 2))) == NULL)
	return (NULL);
      memset(gen.minibuf, 0, READ_SIZE + 2);
    }
  seekline(gen.cursor, gen.buffer, &gen, 1);
  gen.line = malloc(sizeof(char) * seekline(gen.cursor, gen.buffer, &gen, 0));
  dumpline(&gen.cursor, gen.buffer, gen.line);
  if (gen.ret == 0 || gen.ret == -1 || gen.buffer == NULL || gen.line == NULL)
    {
      free(gen.line);
      free(gen.buffer);
      free(gen.minibuf);
      return (NULL);
    }
  return (gen.line);
}

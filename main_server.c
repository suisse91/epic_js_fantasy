/*
** main_sercer.c for server.c in /home/js/last_rush/lib
**
** Made by thibau_j
** Login   <thibau_j@epitech.net>
**
** Started on  Sat May 10 04:27:04 2014 thibau_j
** Last update Sat May 10 15:43:46 2014 ledara_f
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "network_server.h"

int	fd;
t_file	t;

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    i++;
  return (i);
}

void	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}

int     size_tab_struct_champ(t_champ **tab)
{
  int   i;

  i = 1;
  while (tab[i - 1] != NULL)
    ++i;
  return (i);
}

int     size_tab_struct_room(t_room **tab)
{
  int   i;

  i = 1;
  while (tab[i - 1] != NULL)
    ++i;
  return (i);
}

int     size_tab_struct_monster(t_monster **tab)
{
  int   i;

  i = 1;
  while (tab[i - 1] != NULL)
    ++i;
  return (i);
}


void	append_struct_champ(t_champ ***tab, t_champ *champ)
{
  int   i;

  i = size_tab_struct_champ(*tab);
  *tab = realloc(*tab, (i + 1) * sizeof(t_champ *));
  if (*tab == NULL)
    exit(EXIT_FAILURE);
  (*tab)[i - 1] = champ;
  (*tab)[i] = NULL;
}

void	append_struct_room(t_room ***tab, t_room *room)
{
  int   i;

  i = size_tab_struct_room(*tab);
  *tab = realloc(*tab, (i + 1) * sizeof(t_room *));
  if (*tab == NULL)
    exit(EXIT_FAILURE);
  (*tab)[i - 1] = room;
  (*tab)[i] = NULL;
}

void	append_struct_monster(t_monster ***tab, t_monster *monster)
{
  int   i;

  i = size_tab_struct_monster(*tab);
  *tab = realloc(*tab, (i + 1) * sizeof(t_monster *));
  if (*tab == NULL)
    exit(EXIT_FAILURE);
  (*tab)[i - 1] = monster;
  (*tab)[i] = NULL;
}

void	recup_header(char *str, int *i)
{
  int	nb;
  int	j;

  j = 0;
  t.header.magic = str[*i];
  *i = *i + 2;
  if (str[*i] != 1)
    {
      printf("File invalid\n");
      exit(1);
    }
  else
    {
      nb = str[*i + 1];
      *i = *i + 1;
      if ((t.header.name = malloc(nb * sizeof(char))) == NULL)
	exit(1);
      while (nb >= 0)
	{
	  t.header.name[j++] = str[*i];
	  *i = *i + 1;
	  nb--;
	}
      t.header.name[j] = 0;
    }
  my_putstr(t.header.name);
}

void	recup_all(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (str[i] == 123)
	recup_header(str, &i);
      i++;
    }
}

void		function_read(char *buff, size_t size, int id)
{
  char		tab[4096];
  int		ret;

  //  printf("%s\n", buff);
  write(1, "server> ", 8);
  if ((ret = read(fd, tab, 4096)) == -1)
    exit(EXIT_FAILURE);
  tab[ret] = '\0';
  recup_all(tab);
  my_putstr("\n\n\n\n");
  //  write_on_client(tab, strlen(tab), id);
  /*  close_server(); */
}

int	main(int ac, char **argv)
{
  if (ac == 1)
    {
      printf("Missing parameters\n");
      exit(1);
    }
  if ((fd = open(argv[1], O_RDONLY, S_IRWXU)) == -1)
    {
      printf("Open failed, exiting\n");
      exit(1);
    }
  server_loop(0, &function_read);
}

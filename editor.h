/*
** editor.h for epic_editor in /home/elkaim_r/Documents/epic_js_fantasy
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Sat May 10 15:21:05 2014 elkaim_r
** Last update Sat May 10 16:37:40 2014 elkaim_r
*/

#ifndef EDITOR_H_
# define EDITOR_H_

typedef struct		s_header
{
  char			*name;
  char			*end_room;
  char			*start_room;
}			t_header;

typedef struct		s_champ
{
  char			*name;
  char			*type;
  int			hp;
  int			spe;
  int			speed;
  int			deg;
  char			*weapon;
  char			*armor;
  struct s_champ	*next;
}			t_champ;

typedef struct		s_monster
{
  char			*type;
  int			hp;
  int			spe;
  int			speed;
  int			deg;
  char			*weapon;
  char			*armor;
  struct s_monster	*next;
}			t_monster;

typedef struct		s_room
{
  char			*name;
  char			*adv;
  char			*tab_connection;
  char			*tab_monster;
  struct s_room		*next;
}			t_room;

typedef struct		s_game
{
  t_header		*head;
  t_champ		*champ;
  t_monster		*monster;
  t_room		*room;
}			t_game;

/*
** list.c
*/

void		init_header(t_header *);
t_champ		*add_elem_champ(t_champ *);
t_monster	*add_elem_monster(t_monster *);
t_room		*add_elem_room(t_room *);

#endif /* !EDITOR_H_ */

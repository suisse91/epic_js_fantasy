/*
** my_strcmp.c for my_strcmp in /home/elkaim_r/rendu/Piscine-C-Jour_06
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Mon Oct  7 18:29:47 2013 elkaim_r
** Last update Sat May 10 14:14:02 2014 ledara_f
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] == s2[i] && s1[i] != 0 && s2[i] != 0)
    i = i + 1;
  return (s1[i] - s2[i]);
}

int     my_strncmp(char *s1, char *s2, int n)
{
  int   i;

  i = 0;
  while (s1[i] != 0 && s2[i] != 0 && s1[i] == s2[i] && i < (n - 1))
    i = i + 1;
  return (s1[i] - s2[i]);
}

